(function () {
    var data = function (json, screenIndex) {
        var back = $("<button>").attr("class", "back")
            .click(function () {
                $("#screen-" + screenIndex).remove();
            })
            .text("BACK");
        var exit = $("<button>").attr("class", "exit")
            .click(function () {
                for (var i = 1; i <= screenIndex; i++)
                    $("#screen-" + i).remove();
            })
            .text("exit");

        var img = $("<img>").attr("src", json.background)
            .attr("class", "background");
        var svg = $("<div>").attr("class", "svg")
            .load(json.svg, function () {
                $(json.scenes).each(function (index, scene) {
                    $('#' + scene.id).click(function () {
                        data(scene.content, screenIndex + 1);
                    })
                })
            });

        var textDiv = $("<div>").attr("class", "text");

        $(json.texts).each(function (index, text) {
            $("<div>").attr("class", text.class)
                .html(text.value)
                .appendTo(textDiv);
        });

        $("<div>").attr("id", "screen-" + screenIndex)
            .append(img)
            .append(svg)
            .append(textDiv)
            .append(back)
            .append(exit)
            .appendTo("#interactive_board");
    };
    $.getJSON("config.json", function (json) {
        data(json, 0);
    });
})();